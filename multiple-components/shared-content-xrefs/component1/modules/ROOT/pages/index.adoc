= {page-component-title}

== Overall note on structure

This project demonstrates how to share multiple pages between projects or components, and how to set up xrefs between the pages.
For simplicity everything is in one project, however a more likely scenario would be two projects, `component1` plus `shared` and `component2` plus `shared`.
The `confusion` component is included to demonstrate that it is possible to have different directory structures between the partials in the shared component and the consuming component.

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}

== Shared content with xrefs

Component 1 includes the three well-organized pages from the `shared` component in a similar structure as in the source component.
As these pages are not intended to appear standalone, they are `partials` in the `shared` component.
The shared component has these pages in the `ROOT` module, and they are included into the `ROOT` module in this component.
Each page has three links to each of the other two pages:

* To the page itself.
* To the section for the links to the first of the linked pages.
* To the section for the links to the second of the linked pages.

The links between pages use the coordinates of the page expected to include them, although the links are in the included pages.
As a partial is not itself a page, it is not possible for an xref to point to a partial itself.
Instead, it can point to a page that includes the partial.
