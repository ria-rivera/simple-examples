= {page-component-title}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}

page-aliases: (none)

== Page Aliases

The page-aliases project demonstrates all possible forms of page references in the page-aliases attribute

