= Included page

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative} 

== Inclusion stubs and Navigation

Note what happens to the nav tree depending on which flow this is selected from.

Here are links to the inclusion stubs:

== These always open the nav tree to the expected working flow:

xref::stub1.adoc[xref::stub1.adoc]

xref::stub2.adoc[xref::stub2.adoc]

== There is no way for Antora to decide if this should open broken flow one or flow two:

xref::stub3.adoc[xref::stub3.adoc]
