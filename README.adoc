= Simple Examples

This repository contains simple examples of Antora projects showing single features in isolation. They all use the Antora Default UI.

== Beware

In order to demonstrate many features in minimal space, all these projects employ a compressed layout. This works, but is unsuitable for any but the smallest projects.  For any real project, at least the playbook project and the content should be in separate git repositories.

== Prerequisites

You must have Antora installed, preferably globally.

Clone this repo locally.

== Usage

* cd `example-directory`
* antora antora-playbook.yml
* Examine the results in build/

== Contributing

* An easy starting point is to use the antora-schematics example schematic to set up your Antora example with the components it needs. The schematic will create the path, put the antora-playbook.yml file there, and put the components there.
* Name the directory to clearly describe the examples purpose.
* Use the pages of the example to clearly describe the effects of the demonstrated configuration.

```
antora-schematics example --gitPath=- --path=<path-to-example> --components=<comma-separated list of component paths> [--authorName=<git Author> --authorEmail=<gitEmail>]
```

Specifying "`--authorName=<git Author> --authorEmail=<gitEmail>`" once will put them in .git/config and will be used for subsequent isomorphic-git work.
In any case, all new files should be added: if the name/email configuration is present they will also be committed.
To avoid any git activity, use the -noGit=true flag.

== To build everything:

----
find . -name antora-playbook.yml|xargs -n 1 antora
----

== To clean
----
find . -name build |xargs rm -rf
----

=== Catalog

The links will only work locally if you view this page in an asciidoc enabled browser, after building all the projects.

* simplest: minimal example that produces a site with content and navigation link:simplest/build/site/simplest-component/1.0/index.html[]. Note there is no site start_page.
//* tiny: adds a site start page, site.xml, robots.txt
* One component: one-components
** Topics compared to modules, resulting in the same paths link:one-components/topic-module/build/site/index.html[]
** A component may lack a ROOT module link:one-components/no-root/build/site/index.html[]
** Including hidden files whose names start with '_' underscore link:one-components/underscore-include/build/site/index.html[]
** Including the same content in multiple nav positions or flows using include stubs link:one-components/multiple-uses/build/site/index.html[]
** Fragments in nav files link:one-components/nav-fragments/build/site/index.html[]
* Multiple components: multiple-components
** Two components link:multiple-components/two-components/build/site/index.html[]
** Links between two components link:multiple-components/links-between-components/build/site/index.html[]
** Version sorting, prerelease, and versionless for two similar copies of one component, one with plain versions and one with explicit display versions link:multiple-components/versions/build/site/index.html[]
** Xref examples showing the 8 possibilities and their effects link:multiple-components/xrefs/build/site/index.html[]
** Page Alias examples showing all the possible page-aliases specifications link:multiple-components/page-aliases/build/site/index.html[]
** Include examples, mostly showing including 'examples' content link:multiple-components/includes/build/site/index.html[]
** Including shared content into several components demonstrating xrefs between partials link:multiple-components/shared-content-xrefs/build/site/index.html[]
* Distributed components: distributed-components
** One component, two source trees link:distributed-components/simple-distributed/build/site/index.html[]
** One component, two source trees, and each of three modules in one of the sources link:distributed-components/module-per-source/build/site/index.html[]
* Extensions: extensions
** Using the @djencks/asciidoctor-mathjax.js extension for server-side stem rendering link:extensions/mathjax-extension/build/site/index.html[]
** Using the @djencks/asciidoctor-antora-indexer extension to create index lists and tables of pages selected from the Antora content catalog link:extensions/index-extension/build/site/index.html[]
** Using the asciidoctor-kroki extension, demonstrating especially inline/interactive svg link:extensions/kroki-extension/build/site/index.html[]
** Using the @djencks/asciidoctor-template extension for block and inline templates link:extensions/template-extension/build/site/index.html[].
